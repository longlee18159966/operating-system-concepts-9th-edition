#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched/signal.h>
#include <linux/proc_fs.h>

int simple_init (void)
{
    struct task_struct *task;
    printk(KERN_INFO "Init\n");
    for_each_process(task){
        printk(KERN_INFO "%s %ld  %d", task->comm, task->state, task->pid);
    }

    return 0;
}

void simple_exit (void)
{
    printk(KERN_INFO "Exit\n");
}

/* Macros for registering module entry and exit points */
module_init(simple_init);
module_exit(simple_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Simple Module");
MODULE_AUTHOR("LONG");


