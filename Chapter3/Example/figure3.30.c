#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int value = 5;

int main (int argc, char *argv[])
{
	pid_t pid = fork();

	if (pid == 0)
	{
		value += 15;
		printf("%d\n", value);
	}
	else if (pid > 0)
	{
		wait(NULL);
		printf("%d\n", value);
		return 0;
	}
}
